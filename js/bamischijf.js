$(function ($) {
  var max = parseInt($('#max').html(), 10),
    calories = 0,
    storage = new Storage(),
    selection = (storage.get('selection') === null) ? [] : storage.get('selection'),
    snacks = $('.snack').draggable({
      helper: 'clone'
    }),
    plate = $('#plate');

  if (selection.length !== 0) {
    for (var i in selection) {
      if (selection.hasOwnProperty(i)) {
        for (var index in selection[i]) {
          if (selection[i].hasOwnProperty(index)) {
            addSnackToPlate(index, selection[i][index]);
          }
        }
      }

    }
    updateScoreBoard();
  }


  plate.droppable({
    drop: function (event, ui) {
      if (allowedToEat()) {
        var $el = $(ui.draggable);
        var snack = {};
        snack[$el.html()] = $el.data('calories');
        selection.push(snack);
        updateScoreBoard();
        addSnackToPlate($el.html(), $el.data('calories'));
      }

    },
    out: function (event, ui) {
      var $el = $(ui.draggable.context);
      for (var i in selection) {
        if (selection.hasOwnProperty(i)) {
          if (selection[i].hasOwnProperty($el.html())) {
            delete selection[i][$el.html()];
            break;
          }
        }
      }

      setTimeout(function () {
        $el.remove();
      }, 250);

      updateScoreBoard();
    }
  });

  function allowedToEat() {
    return calories < max;
  }

  function updateScoreBoard() {
    var total = $('#total'),
      message = $('#message');
    calories = 0;
    for (var i in selection) {
      if (selection.hasOwnProperty(i)) {
        for (var index in selection[i]) {
          if (selection[i].hasOwnProperty(index)) {
            calories += selection[i][index];
          }
        }
      }
    }
    total.html(calories);
    storage.set('selection', selection);
  }

  function addSnackToPlate(name, calories) {
    console.log(arguments);
    var newEl = $('<div></div>').addClass("snack ui-widget-content").html(name).data('calories', calories).draggable();
    // var newEl = $.clone().draggable();
    plate.append(newEl);
  }
});

function Storage() {
  var self = this;

  self.set = function (key, data) {
    localStorage.setItem(key, JSON.stringify(data))
  };

  self.get = function (key) {
    return JSON.parse(localStorage.getItem(key));
  };

  self.remove = function (key) {
    localStorage.removeItem(key);
  }
}
