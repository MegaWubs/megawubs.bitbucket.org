$(function ($) {
  var quote = new RandomQuote();
  var flickr = new Flicker("713e1de33b94aac4ae172ee7501f89dd");

  (function () {
    var $background = $('.background');
    quote.get().done(function (quote) {
      flickr.getImagesForQuote(quote).done(function (images) {
        var $image = $("<img>").attr('src', images[Math.floor(Math.random() * images.length)].url).addClass('fade-in').on('load', function () {
          console.log('image fully loaded!');
          $background.append($image);
          $("#quote").html(quote.content + "<p>— " + quote.title + "</p>");
        });
      });
    });
    setTimeout(arguments.callee, 30000);
  })();


// implementation
  function RandomQuote() {
    var self = this;

    self.get = function () {
      var dfd = new $.Deferred();
      $.getJSON("http://cors.io/?u=http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1&callback=", function (a) {
        dfd.resolve(a[0]);
      });
      return dfd.promise();
    };

    return self;
  }

  function Flicker(key) {
    var self = this;

    self.getImagesForQuote = function (quote) {
      quote.content = quote.content.replace(/<(?:.|\n)*?>/gm, '');
      var dfd = new $.Deferred(),
        words = [];
      words = filterQuote(quote.content, 4);
      words.push(quote.title);

      getImagesForWords(words).done(function (urls) {
        dfd.resolve(urls);
      });
      return dfd.promise();
    };
    var actions = {
        search: "flickr.photos.search",
        size: "flickr.photos.getSizes"
      },
      defaultSearchImageUrlSettings = {
        minHeight: 720,
        maxHeight: 1080
      },
      url = "https://api.flickr.com/services/rest/?jsoncallback=?",
      search = function (word, words) {
        var dfd = new $.Deferred();
        $.getJSON(url, {
          text: word,
          tags: words.concat(','),
          format: "json",
          api_key: key,
          method: actions.search,
          content_type: 1,
          media: 'photos'
        }).done(function (data) {
          dfd.resolve(
            {
              word: word,
              id: data.photos.pages === 0 ? data : data.photos.photo[0].id
            }
          );
        });
        return dfd;
      },

      filterQuote = function (quote, maxWordLength) {
        return quote.split(' ').filter(function (item) {
          return item.length > maxWordLength;
        });
      },
      getImagesUrl = function (images, settings) {
        var dfd = new $.Deferred(),
          urlFetchers = [];
        for (var i in images) {
          if (images.hasOwnProperty(i)) {
            urlFetchers.push(getImageUrl(images[i], settings))
          }
        }

        $.when.apply($, urlFetchers).done(function () {
          var images = $.grep(arguments, function (image) {
            return image.url !== "";
          });
          dfd.resolve(images);
        });

        return dfd.promise();
      },

      getImageUrl = function (image, settings) {
        var dfd = new $.Deferred();
        settings = $.extend({}, defaultSearchImageUrlSettings, settings);
        $.getJSON(url, {
          method: actions.size,
          api_key: key,
          photo_id: image.id,
          format: 'json'
        }).done(function (data) {
          if (data.stat == 'fail') {
            dfd.resolve(image);
          }
          else {
            var matches = data.sizes.size.filter(function (item) {
              return (item.height <= settings.maxHeight && item.height >= settings.minHeight);
            });
            image.url = matches.length > 0 ? matches.pop().source : "";
            dfd.resolve(
              image
            );
          }

        });

        return dfd;
      },

      getImagesForWords = function (words) {
        var dfd = new $.Deferred(),
          searchers = [];
        for (var i in words) {
          if (words.hasOwnProperty(i)) {
            searchers.push(search(words[i], words));
          }
        }
        $.when.apply($, searchers).done(function () {
          getImagesUrl(arguments, {}).done(function (results) {
            dfd.resolve(results);
          });
        });

        return dfd.promise();
      };
    return self;
  }
});

